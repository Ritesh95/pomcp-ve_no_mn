#include "node.h"
#include "history.h"
#include "utils.h"

using namespace std;

//-----------------------------------------------------------------------------

int VNODE::numFactors = 0;

int **VNODE::sizeofFactorTables = 0;

MEMORY_POOL<VNODE> VNODE::VNodePool;

void VNODE::Initialise()
{
    Count=0;
    assert(numFactors);
    factorQueryVals.resize(numFactors);
    factorInviteVals.resize(numFactors);
    for (int i=0;i<numFactors;i++)
    {
        factorQueryVals[i].resize(sizeofFactorTables[i][0]);
        factorInviteVals[i].resize(sizeofFactorTables[i][1]);
    }
}

VNODE* VNODE::Create()
{
    VNODE* vnode = VNodePool.Allocate();
    vnode->Initialise();
    return vnode;
}

VNODE* VNODE::Create(STATE* state)
{
    VNODE* vnode = VNodePool.Allocate();
    vnode->Initialise();

    vnode->BeliefState.AddSample(state);
    return vnode;
}

void VNODE::Free(VNODE* vnode, const SIMULATOR& simulator)
{
    vnode->BeliefState.Free(simulator);

    for (int i=0;i<numFactors;i++)
    {
        vnode->factorQueryVals[i].clear();
        vnode->factorInviteVals[i].clear();
    }
    vnode->factorQueryVals.clear();
    vnode->factorInviteVals.clear();

    for (map<pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>, VNODE * >::iterator it=vnode->children.begin(); it!=vnode->children.end(); it++)
    {
        VNODE *child = it->second;
        if (child)
            Free(child, simulator);
    }
    
   
    vnode->children.clear();
    VNodePool.Free(vnode);
}

void VNODE::FreeAll()
{
	VNodePool.DeleteAll();
}

void VNODE::SetChildren(int count, double value)
{
    endSessionVal.Set(count,value);
    for (int i=0;i<numFactors;i++)
    {
        vector<VALUE<int> > currQueryFactorStats = factorQueryVals[i];
        for (int j=0;j<currQueryFactorStats.size();j++)
            currQueryFactorStats[j].Set(count, value);

        vector<VALUE<int> > currInviteFactorStats = factorInviteVals[i];
        for (int j=0;j<currInviteFactorStats.size();j++)
            currInviteFactorStats[j].Set(count, value);
    }
}


void VNODE::SetEndSessionChildren(int count, double value)
{
    endSessionVal.Set(0, 0);
    for (int i=0;i<numFactors;i++)
    {
        vector<VALUE<int> > currQueryFactorStats = factorQueryVals[i];
        for (int j=0;j<currQueryFactorStats.size();j++)
            currQueryFactorStats[j].Set(count, value);

        vector<VALUE<int> > currInviteFactorStats = factorInviteVals[i];
        for (int j=0;j<currInviteFactorStats.size();j++)
            currInviteFactorStats[j].Set(count, value);
    }
}

//-----------------------------------------------------------------------------
