#ifndef __POMDP__Graph__
#define __POMDP__Graph__

#include <iostream>
#include<map>
#include<string>

using namespace std;

class Graph{
public:
    string graphfile;
    int numNodes;
    int numEdges;
    int numUncertainEdges;
    double existenceProb;
    double propagationProb;
    
    int T; ////the time t used in the diffusion centrality calculation

    bool isUncertain;
    
    double **adjMatrix;

    
    /////increment edge number only in case there is an uncertain edge
    map<int, map<int, int> * > adjList;
    
    map<int, pair<int, int> > uncertainEdgeList;
    
    Graph(int nodes, int edges);
    
    Graph(string graphFile, bool uncertain, double existenceProb, double
propagationProb);

    void makeUndirected();
    int numUndirectedEdges();
    
    Graph();

    ~Graph();
};

#endif /* defined(__POMDP__Graph__) */
