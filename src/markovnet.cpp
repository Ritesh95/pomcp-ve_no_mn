#include "markovnet.h"
#include <map>
#include <stdlib.h>
#include <algorithm>
#include <assert.h>

#include <iostream>

FACTOR::FACTOR()
{
	scope.clear();
	potential.clear();	
}

float FACTOR::get_factor_value(bool* asgn) const
{
	unsigned index = 0;
	unsigned scope_size = scope.size();
	for(unsigned i = 0; i < scope_size; ++i)
	{
		index = (index << 1) + asgn[scope[i]];
	}

	return potential[index];
}

void FACTOR::print() const
{
	unsigned scope_size = scope.size();
	std::cout << "Scope (size = " << scope_size << "): ";
	for(unsigned i = 0; i < scope_size; ++i)
		std::cout << scope[i] << " ";
	std::cout << std::endl;

	unsigned potential_size = (1 << scope_size);
	std::cout << "Potential: ";
	for(unsigned i = 0; i < potential_size; ++i)
		std::cout << potential[i] << " ";
	std::cout << std::endl;
}

void FACTOR::Free()
{
	scope.clear();
	potential.clear();	
}

MARKOV_NET::MARKOV_NET()
{
	num_vars = 0;
	Factors.clear();
	observed.clear();
}

MARKOV_NET::MARKOV_NET(const MARKOV_NET& mn)
{
	num_vars = mn.num_vars;
	observed = mn.observed;

	Factors.clear();
	unsigned factors_num = mn.Factors.size();
	for(unsigned i = 0; i < factors_num; ++i)
	{
		FACTOR* newfactor = new FACTOR();
		newfactor->scope = mn.Factors[i].scope;
		newfactor->potential = mn.Factors[i].potential;

		Factors.push_back(*newfactor);
	}
}


bool* MARKOV_NET::sample_net(unsigned n_steps) const
{
	std::vector<std::vector<const FACTOR*> > var_factors;	// Map from variable to list of factors containing that variable
	for(unsigned i = 0; i < num_vars; ++i)
	{
		std::vector<const FACTOR*> new_vec;
		var_factors.push_back(new_vec);	// Initializing empty vector for each variable
	}

	unsigned factors_num = Factors.size();
	for(unsigned i = 0; i < factors_num; ++i)	// Iterating over factors and populating var_factors
	{
		unsigned factor_scope_size = Factors[i].scope.size();
		for(unsigned j = 0; j < factor_scope_size; j++)	// Adding this factor to each variable in its scope
			var_factors[Factors[i].scope[j]].push_back(&(Factors[i]));
	}

	// Testing
	// for(unsigned i = 0; i < num_vars; ++i)
	// {
	// 	std::cout << i << std::endl;
	// 	std::vector<FACTOR*> this_factors = var_factors[i];
	// 	for(std::vector<FACTOR*>::iterator it = this_factors.begin(); it != this_factors.end(); ++it)
	// 	{
	// 		for(std::vector<unsigned>::iterator it2 = (*it)->scope.begin(); it2 != (*it)->scope.end(); ++it2)
	// 			std::cout << *it2 << " ";
	// 		std::cout << std::endl;
	// 	}

	// 	std::cout << std::endl;
	// }

	bool* asgn = new bool[num_vars];

	std::vector<unsigned> unobs;	// Vector of unobserved variable indices
	for(unsigned i = 0; i < num_vars; ++i)
		unobs.push_back(i);

	// Setting values of the observed variables & deleting them from unobs
	unsigned observed_size = observed.size();
	for(unsigned i = 0; i < observed_size; ++i)
	{
		asgn[observed[i].first] = observed[i].second;

		std::vector<unsigned>::iterator pos = std::find(unobs.begin(), unobs.end(), observed[i].first);
		if(pos != unobs.end())
			unobs.erase(pos);
	}

	// Setting random values for the unobserved variables (of the initial state)
	for(std::vector<unsigned>::iterator it = unobs.begin(); it != unobs.end(); ++it)
	{
		double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);	// Sample from U[0,1]		
		asgn[*it] = (r < 0.5);
	}

	// std::cout << "Unobserved Variables: ";
	// for(std::vector<unsigned>::iterator it = unobs.begin(); it != unobs.end(); ++it)
	// 	std::cout << *it << " ";
	// std::cout << "\n\n";

	// std::cout << "Observed Variables: ";
	// for(std::vector<std::pair<unsigned, bool> >::iterator it = observed.begin(); it != observed.end(); ++it)
	// 	std::cout << "(" << it->first << "," << it->second << ") ";
	// std::cout << "\n\n";

	unsigned unobs_num = unobs.size();

	for(unsigned t = 0; t < n_steps; t++)
	{
		int var = unobs[rand() % unobs_num];	// Choose variable to sample

		// Sample value from X_var | X_{-var} = x_{-var}
		std::vector<const FACTOR*>* this_factors = &var_factors[var];	// Vector of factors containing X_var
		//

		asgn[var] = 0;	// Temporarily setting value to 0
		float prob0 = 1.0;
		for(std::vector<const FACTOR*>::iterator it = this_factors->begin(); it != this_factors->end(); ++it)	// Computing unnormalized probability with value 0
			prob0 *= (*it)->get_factor_value(asgn);

		asgn[var] = 1;
		float prob1 = 1.0;
		for(std::vector<const FACTOR*>::iterator it = this_factors->begin(); it != this_factors->end(); ++it)	// Computing unnormalized probability with value 1
			prob1 *= (*it)->get_factor_value(asgn);

		float sum_probs = prob0 + prob1;	// Normalization constant
assert(sum_probs > 0);
		prob0 = prob0/sum_probs;
		prob1 = prob1/sum_probs;

		double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);	// Sample from U[0,1]		
		if(r < prob0)
			asgn[var] = 0;
		else
			asgn[var] = 1;
	}

	return asgn;
}

void MARKOV_NET::add_observation(unsigned var, bool val)
{
	observed.push_back(std::make_pair(var,val));
}

void MARKOV_NET::print() const
{
    std::cout << "Markov Network number of variables: " << num_vars << std::endl;

    std::cout << "Markov Network observed variables: ";
    unsigned observed_size = observed.size();
    for(unsigned i = 0; i < observed_size; ++i)
        std::cout << observed[i].first << ':' << observed[i].second << ' ';
    std::cout << std::endl;

    std::cout << "Markov Network potentials:\n";
    unsigned factors_num = Factors.size();
    for(unsigned i = 0; i < factors_num; ++i)
        Factors[i].print();
    std::cout << std::endl;
}

void MARKOV_NET::Free()
{
	observed.clear();

	for(std::vector<FACTOR>::iterator it = Factors.begin(); it != Factors.end(); ++it)
		it->Free();
	Factors.clear();
}
