#include <iostream>
#include <boost/multiprecision/cpp_int.hpp>

int main()
{
	boost::multiprecision::cpp_int temp;
	temp = 1;
	std::cout << temp << std::endl;

	std::cout << (temp<<100) <<std::endl;
	std::cout << (temp<<200) <<std::endl;

	return 0;
}
