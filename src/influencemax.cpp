#include "influencemax.h"
#include "beliefstate.h"
#include "utils.h"
#include <vector>
#include <time.h>

using namespace std;
using namespace UTILS;

INFLUENCEMAX::INFLUENCEMAX(unsigned num_nodes, unsigned max_per_query, unsigned max_invite, unsigned total_sessions, unsigned max_actions, MARKOV_NET mn, double query_cost, double invite_cost, Graph* graph)
{
	srand(time(NULL));
	NUM_NODES = num_nodes;
	MAX_PER_QUERY = max_per_query;
	MAX_INVITE = max_invite;
	TOTAL_SESSIONS = total_sessions;
	MAX_ACTIONS = max_actions;
	MN = mn;
	global_graph = graph;

	NumActions = 0;
	for(int i = 1; i <= MAX_PER_QUERY; ++i)	// Counting query actions
		NumActions += combination(NUM_NODES, i);
	for(int i = 1; i <= MAX_INVITE; ++i)	// Counting invite actions
		NumActions += combination(NUM_NODES, i);
	NumActions += 1;	// +1 for EndSession action

	int max_obs_size = max(MAX_PER_QUERY, MAX_INVITE);
	NumObservations = 0;
	for(int i = 1; i <= max_obs_size; i++)	// Adding count for each size of obs possible
		NumObservations += (1 << i);
	NumObservations += 1;	// Null observation on EndSession action or null invite/query

	QUERY_COST = query_cost;
	INVITE_COST = invite_cost;

	Discount = 1.0;
	RewardRange = NUM_NODES;	// Using number of nodes as the exporation scaling parameter
}

STATE* INFLUENCEMAX::Copy(const STATE& state) const
{
	assert(state.IsAllocated());
	const INFLUENCEMAX_STATE& oldstate = safe_cast<const INFLUENCEMAX_STATE&>(state);
	INFLUENCEMAX_STATE* newstate = MemoryPool.Allocate();

	newstate->presence = new bool[NUM_NODES];
	for(int i = 0; i < NUM_NODES; ++i)
		newstate->presence[i] = oldstate.presence[i];

	newstate->invited = oldstate.invited;
	newstate->actions_left = oldstate.actions_left;
	newstate->session_index = oldstate.session_index;

	return newstate;
}

void INFLUENCEMAX::Validate(const STATE& state) const
{
	const INFLUENCEMAX_STATE& imstate = safe_cast<const INFLUENCEMAX_STATE&>(state);

	if(imstate.invited.size() > MAX_INVITE)
	{
		DisplayState(imstate, cout);
		assert(false);
	}

	if(imstate.actions_left > MAX_ACTIONS)
	{
		DisplayState(imstate, cout);
cout << "actions_left gone negative!" << endl;
exit(0);
		assert(false);
	}

	if(imstate.session_index > TOTAL_SESSIONS + 1)
	{
		DisplayState(imstate, cout);
		assert(false);
	}
}

STATE* INFLUENCEMAX::CreateStartState() const
{
	INFLUENCEMAX_STATE* imstate = MemoryPool.Allocate();
	imstate->presence = MN.sample_net();
	imstate->invited.clear();
	imstate->actions_left = MAX_ACTIONS;
	imstate->session_index = 1;

	return imstate;
}

void INFLUENCEMAX::FreeState(STATE* state) const
{
	INFLUENCEMAX_STATE* imstate = safe_cast<INFLUENCEMAX_STATE*>(state);
	MemoryPool.Free(imstate);
}

bool INFLUENCEMAX::LocalMove(STATE& state, const HISTORY& history, int stepObs, const STATUS& status) const
{
  return true;
}

bool INFLUENCEMAX::Step(STATE& state, boost::multiprecision::cpp_int action, boost::multiprecision::cpp_int& observation, double& reward) const
{
	INFLUENCEMAX_STATE& imstate = safe_cast<INFLUENCEMAX_STATE&>(state);

	bool* action_nodes = new bool[NUM_NODES];
	unsigned action_type = decipher_action(action, action_nodes);

	if(action_type == 2)	// endSession action
	{
		imstate.presence = MN.sample_net();	// Sampling from markov net for people in new sessions
		// Invited set does not change
		imstate.actions_left = MAX_ACTIONS;	// Refreshing action quota
		imstate.session_index += 1;	// Advancing to next sessions;

		reward = 0;	// No reward to move to next session
		observation = 0;	// Null observation on taking endSession

		if(imstate.session_index == TOTAL_SESSIONS+1)	// Last session completed
		{
            double MC_Sims = 1000;
    
            double avgInfSpread=0;
            for (double sims=0;sims<MC_Sims;sims++)
            {
                bool *state1 = new bool[NUM_NODES];
                for (int i=0;i<NUM_NODES;i++)
                    state1[i] = 0;
		for(set<int>::iterator it = imstate.invited.begin(); it != imstate.invited.end(); it++)
			state1[*it] = 1;

                bool *newState = new bool[NUM_NODES];
                for (int i=0;i<global_graph->T;i++)
                {
                    for (int j=0;j<NUM_NODES;j++)
                        newState[j] = 0;
                    for (int j=0;j<NUM_NODES;j++)
                    {
                        if (state1[j])//node i will spread influence
                        {
                            for (int k=0;k<NUM_NODES;k++)
                            {
                                if (j!=k && global_graph->adjMatrix[j][k]!=0 && !newState[k] && !state1[k])//k is uninfluenced neighbor
                                {
                                    double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                                    if (r < global_graph->existenceProb * global_graph->propagationProb)
                                        newState[k]=1;
                                    else
                                        newState[k]=0;
                                }
                            }
                        }
                    }
                    for (int j=0;j<NUM_NODES;j++)
                        if (newState[j])
                            state1[j]=1;    
                }

                double infSpread = 0;
                for (int i=0;i<NUM_NODES;i++)
                    infSpread += state1[i];

                avgInfSpread += infSpread;
            }

            reward = avgInfSpread/MC_Sims;

            //reward += ((double)(MAX_INVITE - imstate.invited.size()))*(-1000);

//cout << "REWARD: " << reward << endl;
        
			return true;
		}
	}

	vector<unsigned> activeNodes;	// vector of nodes queried/invited
	for(int i = 0; i < NUM_NODES; ++i)
	{
		if(action_nodes[i])
			activeNodes.push_back(i);
	}

	if(action_type == 0)	// Query Action
	{
		assert(imstate.actions_left > 0);

		// Presence does not change
		// Invited set does not change
		imstate.actions_left -= 1;
		// Session index does not change

		unsigned obser_size = activeNodes.size();
		assert(obser_size <= MAX_PER_QUERY);

		unsigned* obser = new unsigned[obser_size];	// observation in vector form
		for(int i = 0; i < obser_size; ++i)
			obser[i] = imstate.presence[activeNodes[i]];

		observation = 0;	// observation in index form
		for(int i = 0; i < obser_size; ++i)
			observation += (obser[i] << i);

		reward = -(QUERY_COST * obser_size);	// query cost per node * number of nodes queried
	}

	else if(action_type == 1)	// Invite action
	{
		assert(imstate.actions_left > 0);

		// Presence does not change
		imstate.actions_left -= 1;
		// Session index does not change

		unsigned obser_size = activeNodes.size();
		assert(obser_size + imstate.invited.size() <= MAX_INVITE);

		bool* obser = new bool[obser_size];	// observation in vector form
		for(int i = 0; i < obser_size; ++i)
		{
			if(imstate.presence[activeNodes[i]])
			{
				obser[i] = 1;
				imstate.invited.insert(activeNodes[i]);
			}
			else
				obser[i] = 0;
		}

		observation = 0;	// observation in index form
		for(int i = 0; i < obser_size; ++i)
			observation += (obser[i] << i);

		reward = -(INVITE_COST * obser_size);	// invite cost per node * number of nodes invited
	}

	return false;
}

// // THIS WOULD BE TOO COSTLY
// // Assuming access to actionMap here
// void INFLUENCEMAX::GenerateLegal(const STATE& state, const HISTORY& history, vector<int>& legal, const STATUS& status) const
// {
// 	legal.push_back(MAX_ACTIONS-1);	// Pushing back the end_session action

// 	if(state.actions_left == 0)
// 		return;

// 	if(state.invited.size() == MAX_INVITE)
// 		return;

// 	bool* query_invalid = new bool[NUM_NODES];	// vector defining which nodes cannot be queried
// 	bool* invite_invalid = new bool[NUM_NODES];	// vector defining which nodes cannot be invited

// 	for(int i = 0; i < NUM_NODES; i++)	// initializing everything to valid
// 	{
// 		query_invalid[i] = 0;
// 		invite_invalid[i] = 0;
// 	}

// 	for(set<int>::iterator it = state.invited.begin(); it != state.invited.end(); ++it)
// 	{
// 		query_invalid[*it] = 1;	// Already invited node cannot be queried
// 		invite_invalid[*it] = 1;	// Already invited node cannot be invited
// 	}

// 	// Iterate over history and invalidate nodes that have already been queried/invited in the current session
// 	unsigned history_session = 1;
// 	unsigned history_size = history.Size();
// 	for(int t = 0; t < history_size; ++t)
// 	{
// 		assert(history_session <= state.session_index);

// 		ENTRY& e = history[t];

// 		if(e.Action == MAX_ACTIONS-1)	// if it's the end session action
// 		{
// 			histroy_session += 1;
// 			continue;
// 		}

// 		if(history_session == state.session_index)	// if t of the history is of same session as the current one
// 		{
// 			map<int, ACTION>::iterator act_it = actionMap.find(e.Action);
// 			assert(act_it->second.type != 2);

// 			if(act_it->second.type == 0)	// Queried node cannot be queried again
// 			{
// 				for(int i = 0; i < NUM_NODES; ++i)
// 				{
// 					if(act_it->second.nodes[i])
// 					{
// 						query_invalid[i] = 1;
// 						if(state.presence[i] == 0)	// if queried and confirmed absence
// 							invite_invalid[i] = 1;
// 					}
// 				}
// 			}

// 			else(act_it->second.type == 1)	// Invited node cannot be queried or invited again
// 			{
// 				for(int i = 0; i < NUM_NODES; ++i)
// 				{
// 					if(act_it->second.nodes[i])
// 					{
// 						query_invalid[i] = 1;
// 						invite_invalid[i] = 1;
// 					}
// 				}
// 			}
// 		}
// 	}

// 	// Use query_invalid and invite_invalid to construct legal! (Also remember that invite limit would be MAX_INVITE - state.invited.size())
// }

// Picks a (full) invite action uniformly at random; if not possible, then endSession
boost::multiprecision::cpp_int INFLUENCEMAX::SelectRandom(const STATE& state, const HISTORY& history, const STATUS& status) const
{
/* boost::multiprecision::cpp_int action_id2;
action_id2 = 1;
action_id2 = (action_id2 << (NUM_NODES+1));       // endSession action
return action_id2;*/

	const INFLUENCEMAX_STATE& imstate = safe_cast<const INFLUENCEMAX_STATE&>(state);
	boost::multiprecision::cpp_int action_id;

	////// (1 << 100) doesn't work; Not sure what done below will work
	if(imstate.actions_left == 0)	// Cannot invite anymore
	{
		action_id = 1;
		action_id = (action_id << (NUM_NODES+1));	// endSession action
		return action_id;
	}

	unsigned k = (MAX_INVITE - imstate.invited.size());	// Invite as many as possible (randomly)

	if(k == 0)	// No more nodes can be invited
	{
		action_id = 1;
		action_id = (action_id << (NUM_NODES+1));	// endSession action
		return action_id;
	}

	set<unsigned> selected;

	while(selected.size() < k)
	{
		unsigned sample = rand() % NUM_NODES;
		selected.insert(sample);
	}

	// Convert selected set of nodes into action
	action_id = 1;
	boost::multiprecision::cpp_int temp_cppint = 1;

	action_id = (action_id << NUM_NODES);	// Making it a invite action
	for(set<unsigned>::iterator it = selected.begin(); it != selected.end(); ++it)	// Setting nodes in action
		action_id += (temp_cppint << *it);	////// Will this work?

	return action_id;
}

void INFLUENCEMAX::Prior(const STATE* state, const HISTORY& history, VNODE* vnode, const STATUS& status) const
{
	const INFLUENCEMAX_STATE& imstate = safe_cast<const INFLUENCEMAX_STATE&>(*state);
 	if((imstate.actions_left == 0) || (imstate.invited.size() == MAX_INVITE))
		vnode->SetEndSessionChildren(+LargeInteger, -Infinity);
	else
		vnode->SetChildren(0, 0);
	return;
}


void INFLUENCEMAX::DisplayBeliefs(const BELIEF_STATE& beliefState, ostream& ostr) const
{
	ostr << endl;

	ostr << "Belief is collection of the following particles: " << endl;

	unsigned numSamples = beliefState.GetNumSamples();

	for(int i = 0; i < numSamples; ++i)
	{
		ostr << endl;
		DisplayState(*(beliefState.GetSample(i)), ostr);
	}

	ostr << endl;
}

void INFLUENCEMAX::DisplayState(const STATE& state, ostream& ostr) const
{
	const INFLUENCEMAX_STATE& imstate = safe_cast<const INFLUENCEMAX_STATE&>(state);
	ostr << endl;

	ostr << "Presence: ";
	for(int i = 0; i < NUM_NODES; ++i)
		ostr << imstate.presence[i];
	ostr << endl;

	ostr << "Invited Nodes: ";
	for(set<int>::iterator it = imstate.invited.begin(); it != imstate.invited.end(); ++it)
		ostr << *it << " ";
	ostr << endl;

	ostr << "Actions left: " << imstate.actions_left << endl;
	ostr << "Session index: " << imstate.session_index << endl;
}

void INFLUENCEMAX::DisplayObservation(const STATE& state, boost::multiprecision::cpp_int observation, ostream& ostr) const
{
	ostr << endl;
	
	vector<bool> obser;
	boost::multiprecision::cpp_int temp = observation;
	while(temp > 0)
	{
		obser.push_back(temp%2);
		temp = (temp >> 1);
	}

	unsigned obser_size = obser.size();

	ostr << "Observation: ..";
	for(int i = obser_size-1; i >= 0; i--)
		ostr << obser[i];
	ostr << endl;
}

void INFLUENCEMAX::DisplayAction(boost::multiprecision::cpp_int action, ostream& ostr) const
{
	bool* action_nodes = new bool[NUM_NODES];
	unsigned action_type = decipher_action(action, action_nodes);

	ostr << endl;

	ostr << "Action Type: " << action_type << endl;

	if(action_type != 2)	// Query or invite action
	{
		ostr << "Active Nodes: ";
		for(int i = 0; i < NUM_NODES; ++i)
		{
			if(action_nodes[i])
				ostr << i << " ";
		}
		ostr << endl;
	}		
}

unsigned INFLUENCEMAX::combination(unsigned n, unsigned k)
{
	unsigned prod = 1;
	for(int i = n; i >= n-k+1; i--)
		prod *= i;
	for(int i = 1; i <= k; i++)
		prod = prod/i;
	return prod;
}

unsigned INFLUENCEMAX::decipher_action(boost::multiprecision::cpp_int action, bool* action_nodes) const
{
	boost::multiprecision::cpp_int temp = action;
	for(int i = 0; i < NUM_NODES; ++i)
	{
		action_nodes[i] = (temp%2);
		temp = (temp >> 1);
	}

	unsigned action_type;
	if(temp%2 == 1)
		action_type = 1;	// invite action (bit after nodes is 1)
	else
	{
		temp = (temp >> 1);
		if(temp%2 == 1)
			action_type = 2;	// endSession action (bit after invite bit is 1)
		else
			action_type = 0;	// query action (both bits are 0)
	}

	return action_type;
}
