def construct_reverse_adjacency(U):
    '''
    Converts an adjacency matrix into a reversed adjacency list
    '''
    G = []
    for u in range(U.shape[0]):
        G.append([])
        for v in range(U.shape[0]):
            if U[v,u] != 0:
                G[u].append(v)
    return G

def make_rr_sets(U, P, num_stages, stage_length, num_sets, counted_nodes):
    import numpy as np, random
    '''
    Constructs a random sample of reverse reachability sets. G should be
    a reverse adjacency list representation of the graph. 
    
    Returns num_sets RR sets for each node in the graph.
    '''
    G = construct_reverse_adjacency(U)
    rr_sets = []
    for i in range(num_sets):
        for v in counted_nodes:
            newU = U.copy()
            state = np.zeros((len(G)))
            new_set = [set() for _ in range(num_stages)] #set of nodes reachable at each timestep
            state[v] = 1
            for tprime in range(0, num_stages):
                new_set[tprime].add(v)
            curr_active = [v] #set of active nodes to propagate from
            #run the influence process
            for t in range(num_stages):
                for l in range(stage_length):
                    new_actives = []
                    for u in curr_active:
                        #check if each neighbor can be influenced
                        for w in G[u]:
                            if state[w] == 0:
                                if newU[w,u] < 1 and newU[w,u] > 0:
                                    if random.random() < newU[w,u]:
                                        newU[w,u] = 1
                                    else:
                                        newU[w,u] = 0
                                if newU[w,u] == 1 and random.random() < P[w,u]:
                                    #when a node is influenced, add it to the reachable set for this and 
                                    #all future time steps
                                    state[w] = 1
                                    new_actives.append(w)
                                    for tprime in range(t, num_stages):
                                        new_set[tprime].add(w)
                    curr_active.extend(new_actives)
            rr_sets.append(new_set)
    return rr_sets



def check_rr_intersection(S, rr_sets):
    '''
    Returns the fraction of RR sets which S hits.
    '''
    total = 0
    T = len(S)
    for i, r in enumerate(rr_sets):
        for t in range(T):
            if not S[t].isdisjoint(r[T-t-1]):
                total += 1
                break
    return float(total)/len(rr_sets)

def eval_node_rr(u, S, curr_stage, num_nodes, rr_sets):
    if hasattr(u, '__iter__'):
        S[curr_stage] = S[curr_stage].union(u)
    else:
        S[curr_stage].add(u)
    
    val = check_rr_intersection(S, rr_sets)*num_nodes
    
    if hasattr(u, '__iter__'):
        S[curr_stage].difference_update(u)
    else:
        S[curr_stage].remove(u)
    
    return val


def run_greedy(idx, U, P, num_stages, stage_length, K, num_sets):
    '''
    Runs a greedy policy which uses lazy evaluations across stages. 
    '''
    import heapq
    import random
    
    U = U.copy()    
    num_nodes = U.shape[0]
        
    starting_objective = 0
    
    all_nodes_picked = []
    S = [set() for _ in range(num_stages)]
    
    #each stage, pick K nodes maximizing the marginal gain
    for t in range(num_stages):
        rr_sets = make_rr_sets(U, P, num_stages, stage_length, num_sets, range(num_nodes))      
        upper_bounds = []
#        print('t', t)
        for u in range(num_nodes): #evaluate marginal gain of node u
            if u not in all_nodes_picked:
                upper_bounds.append((starting_objective-eval_node_rr(u, S, t, num_nodes, rr_sets), u))
        #convert to a min-heap        
        heapq.heapify(upper_bounds)
        #pick the nodes
        new_picks = []
        for i in range(K):
#            print('K', i)
            found_next = False
            while not found_next:
                #pop the top element off the heap
                val, u = heapq.heappop(upper_bounds)
                #re-evaluate its marginal gain
                new_val = eval_node_rr(u, S, t, num_nodes, rr_sets) - starting_objective
                #check if it's still the best (within small epsilon)
                if new_val >= -upper_bounds[0][0] - 0.1:
#                    print(new_val)
                    all_nodes_picked.append(u)
                    new_picks.append(u)
                    S[t].add(u)
                    starting_objective = eval_node_rr([], S, t, num_nodes, rr_sets)
                    found_next = True
                else:
                    heapq.heappush(upper_bounds, (-new_val, u))
        #make observations of their edges
        for u in new_picks:
            for v in range(num_nodes):
                if U[u,v] < 1 and U[u,v] > 0:
                    if random.random() <= U[u,v]:
                        U[u,v] = 1
                    else:
                        U[u,v] = 0
#    print(starting_objective)
    return (starting_objective, S, U)

def load(fname, propagation_prob, num_nodes):
    import numpy as np
    #read in the adjacency list
    a = np.loadtxt(fname)
    a[:,:2] -= 1    #convert nodes to 0-indexing
    #num_nodes = len(np.unique(a))
    print num_nodes
    U = np.zeros((num_nodes, num_nodes))  #matrix storing existence probabilities
    for i in range(a.shape[0]):
        U[int(a[i, 0]), int(a[i, 1])] = 1
        #if a[i,2] == 1: #certain edge
        #    U[a[i, 0], a[i, 1]] = 1
        #else:   #uncertain edge
        #    U[a[i, 0], a[i, 1]] = uncertain_prob
    
    #set propagation probabilities for certain and uncertain edges
    P = np.zeros((num_nodes, num_nodes))
    P[U != 0] = propagation_prob
    return (U,P)


from sys import argv

#numFiles=int(argv[1])
numFiles=1
num_vertices = int(argv[1])
#num_communities = int(argv[3])
K = int(argv[2])

#pVal=float(argv[5])
pVal=0.6

SeedSets = []
ObjVals = [] 
for i in range(numFiles):
    print "Here"+str(i)
    (U,P) = load('/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/IMDatasetCode/MFPUse.txt', pVal, num_vertices)
    #(U,P) = load('/Users/amulyayadav/Dropbox/Work/PostAAAI/amulyaya/NetworkX/'+str(num_vertices)+'/'+str(i+1)+'.txt', pVal, num_vertices)
    #(U,P) = load('/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/BTERNetsRitesh/'+str(num_vertices)+'/'+str(i)+'.txt', pVal, num_vertices)
    #(U,P) = load('/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/IMDataset/bter'+str(num_vertices)+'_'+str(num_communities)+'_'+str(i)+'.txt', pVal, num_vertices)
    (obj, S, U) = run_greedy(0, U, P, 1, 1, K, 1000)
    SeedSets.append(S[0])
    ObjVals.append(obj)

AllOutputs = []
for i in range(numFiles):
    temp = list(SeedSets[i])
    temp.append(ObjVals[i])
    AllOutputs.append(temp)

import numpy as np
fmtString = ""
for i in range(K):
    fmtString+="%d "
fmtString+="%.3f"
#np.savetxt('/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/IMDataset/output.txt', AllOutputs, fmt = fmtString)
np.savetxt('/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/IMDatasetCode/MFPResults.txt', AllOutputs, fmt = fmtString)
#np.savetxt('/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/RiteshResults/DOSIMWithoutFailures/'+str(num_vertices)+'.txt', AllOutputs, fmt = fmtString)
