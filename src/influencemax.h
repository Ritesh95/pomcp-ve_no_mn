#ifndef INFLUENCEMAX_H
#define INFLUENCEMAX_H

#include "simulator.h"
#include <set>
#include "markovnet.h"
#include "Graph.h"

class INFLUENCEMAX_STATE : public STATE
{
public:
	bool* presence;
	std::set<int> invited;
	unsigned actions_left;
	unsigned session_index;
};

class INFLUENCEMAX : public SIMULATOR
{
public:
	INFLUENCEMAX(unsigned num_nodes, unsigned max_per_query, unsigned max_invite, unsigned total_sessions, unsigned max_actions, MARKOV_NET mn, double query_cost, double invite_cost, Graph* graph);

	virtual STATE* Copy(const STATE& state) const;
	virtual void Validate(const STATE& state) const;
	virtual STATE* CreateStartState() const;
	virtual void FreeState(STATE* state) const;
	virtual bool Step(STATE& state, boost::multiprecision::cpp_int action, boost::multiprecision::cpp_int& observation, double& reward) const;

	// void GenerateLegal(const STATE& state, const HISTORY& history, std::vector<int>& legal, const STATUS& status) const;
	virtual bool LocalMove(STATE& state, const HISTORY& history, int stepObs, const STATUS& status) const;

	virtual boost::multiprecision::cpp_int SelectRandom(const STATE& state, const HISTORY& history, const STATUS& status) const;

	virtual void Prior(const STATE* state, const HISTORY& history, VNODE* vnode, const STATUS& status) const;

	virtual void DisplayBeliefs(const BELIEF_STATE& beliefState, std::ostream& ostr) const;
	virtual void DisplayState(const STATE& state, std::ostream& ostr) const;
	virtual void DisplayObservation(const STATE& state, boost::multiprecision::cpp_int observation, std::ostream& ostr) const;
	virtual void DisplayAction(boost::multiprecision::cpp_int action, std::ostream& ostr) const;

private:
	unsigned NUM_NODES;
	unsigned MAX_PER_QUERY;
	unsigned MAX_INVITE;
	unsigned TOTAL_SESSIONS;
	unsigned MAX_ACTIONS;

	double QUERY_COST;	// Cost per node queried
	double INVITE_COST;	// Cost per node invited

	MARKOV_NET MN;
    Graph* global_graph;

	// This function computes n choose k
	unsigned combination(unsigned n, unsigned k);

	// This function reads action and populates action_nodes based on which nodes are queried/invited. It also returns the type of the action (0: query, 1: invite, 2: endSesssion)
	unsigned decipher_action(boost::multiprecision::cpp_int action, bool* action_nodes) const;

    mutable MEMORY_POOL<INFLUENCEMAX_STATE> MemoryPool;
};

#endif // INFLUENCEMAX_H
