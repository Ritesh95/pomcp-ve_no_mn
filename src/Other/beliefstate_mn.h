#ifndef BELIEF_STATE_H
#define BELIEF_STATE_H

#include "markovnet.h"
#include <set>

class STATE;
class SIMULATOR;

class BELIEF_STATE
{
public:

    BELIEF_STATE();

    // Free memory for components of BELIEF_STATE
    void Free(const SIMULATOR& simulator);

    // Creates new state, now owned by caller
    STATE* CreateSample(const SIMULATOR& simulator) const;

    // Added state is owned by belief state
    // void AddSample(STATE* state);   // Assuming not used

    // Copy from beliefs to current BELIEF_STATE
    void Copy(const BELIEF_STATE& beliefs, const SIMULATOR& simulator);

    // Move from beliefs to current BELIEF_STATE
    void Move(BELIEF_STATE& beliefs);

    void print() const;

    // bool Empty() const { return Samples.empty(); }   // Assuming not used
    // int GetNumSamples() const { return Samples.size(); } // Assuming not used
    // const STATE* GetSample(int index) const { return Samples[index]; }   // Assuming not used
    
    MARKOV_NET* presence_belief;
    std::set<int> invited;
    unsigned actions_left;
    unsigned session_index;
};

#endif // BELIEF_STATE_H
