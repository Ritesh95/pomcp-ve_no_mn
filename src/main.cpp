#include "mcts.h"
#include "experiment.h"
#include "influencemax.h"
#include "Graph.h"
#include "markovnet.h"
#include <boost/program_options.hpp>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace boost::program_options;

void UnitTests()
{
}

void disableBufferedIO(void)
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);
    setbuf(stderr, NULL);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

void gen_random(char *s, const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}

void writeAdjacencyList(Graph* graph, string filename)
{
        ofstream file;
        file.open(filename.c_str());
        file<<graph->numNodes<<" "<<graph->numUndirectedEdges()<<" 001 \n";
        for (map<int, map<int, int> * >::iterator it=graph->adjList.begin(); it!=graph->adjList.end();it++)
        {
                map<int, int> *nodeList = it->second;
                for (map<int, int>::iterator iter = nodeList->begin(); iter!= nodeList->end(); iter++)
                {
                        if (iter->second==-1)///certain edge
                                file<<iter->first<<" 2 ";
                        else ///uncertain edge
                                file<<iter->first<<" 1 ";
                }
                file<<"\n";
        }
        file.close();
}

string NumberToString ( int Number )
{
        stringstream ss;
        ss << Number;
        return ss.str();
}


int combination(unsigned n, unsigned k)
{
    unsigned prod = 1;
    for(int i = n; i >= n-k+1; i--)
        prod *= i;
    for(int i = 1; i <= k; i++)
        prod = prod/i;
    return prod;
}



int main(int argc, char* argv[])
{
    MCTS::PARAMS searchParams;
    EXPERIMENT::PARAMS expParams;
    SIMULATOR::KNOWLEDGE knowledge;
    string problem, outputfile, policy;
    int size, number, treeknowledge = 1, rolloutknowledge = 1, smarttreecount = 10;
    double smarttreevalue = 1.0;

    double existenceProb, propagationProb;
    string basedir,graphfile, markovnetfile;
    int numPartitions;
    int numSessions;
    int MAX_PER_QUERY, MAX_INVITE, NUM_SESSIONS;

    double querycost, invitecost;
    int actionsinSession;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("test", "run unit tests")
        ("problem", value<string>(&problem), "problem to run")
        ("outputfile", value<string>(&outputfile)->default_value("output.txt"), "summary output file")
        ("policy", value<string>(&policy), "policy file (explicit POMDPs only)")
        ("size", value<int>(&size), "size of problem (problem specific)")
        ("number", value<int>(&number), "number of elements in problem (problem specific)")
        ("timeout", value<double>(&expParams.TimeOut), "timeout (seconds)")
        ("mindoubles", value<int>(&expParams.MinDoubles), "minimum power of two simulations")
        ("maxdoubles", value<int>(&expParams.MaxDoubles), "maximum power of two simulations")
        ("runs", value<int>(&expParams.NumRuns), "number of runs")
        ("accuracy", value<double>(&expParams.Accuracy), "accuracy level used to determine horizon")
        ("horizon", value<int>(&expParams.UndiscountedHorizon), "horizon to use when not discounting")
        ("numSteps", value<int>(&expParams.NumSteps), "number of steps to run when using average reward")
        ("verbose", value<int>(&searchParams.Verbose), "verbosity level")
        ("autoexploration", value<bool>(&expParams.AutoExploration), "Automatically assign UCB exploration constant")
        ("exploration", value<double>(&searchParams.ExplorationConstant), "Manual value for UCB exploration constant")
        ("usetransforms", value<bool>(&searchParams.UseTransforms), "Use transforms")
        ("transformdoubles", value<int>(&expParams.TransformDoubles), "Relative power of two for transforms compared to simulations")
        ("transformattempts", value<int>(&expParams.TransformAttempts), "Number of attempts for each transform")
        ("userave", value<bool>(&searchParams.UseRave), "RAVE")
        ("ravediscount", value<double>(&searchParams.RaveDiscount), "RAVE discount factor")
        ("raveconstant", value<double>(&searchParams.RaveConstant), "RAVE bias constant")
        ("treeknowledge", value<int>(&knowledge.TreeLevel), "Knowledge level in tree (0=Pure, 1=Legal, 2=Smart)")
        ("rolloutknowledge", value<int>(&knowledge.RolloutLevel), "Knowledge level in rollouts (0=Pure, 1=Legal, 2=Smart)")
        ("smarttreecount", value<int>(&knowledge.SmartTreeCount), "Prior count for preferred actions during smart tree search")
        ("smarttreevalue", value<double>(&knowledge.SmartTreeValue), "Prior value for preferred actions during smart tree search")
        ("disabletree", value<bool>(&searchParams.DisableTree), "Use 1-ply rollout action selection")

	    ("basedir", value<string>(&basedir), "base directory")
	    ("graphfile", value<string>(&graphfile), "graph input file")
	     ("markovnetfile", value<string>(&markovnetfile), "markov net input file")
        ("numPartitions", value<int>(&numPartitions), "number of communities")
	    ("existenceProb", value<double>(&existenceProb), "existence probability")
	    ("propagationProb", value<double>(&propagationProb), "propagation probability")
        ("MAX_PER_QUERY", value<int>(&MAX_PER_QUERY), "max query size")
        ("MAX_INVITE", value<int>(&MAX_INVITE), "max invite size")
	("numSessions", value<int>(&NUM_SESSIONS), "number of sessions")
        ("maxActions", value<int>(&actionsinSession), "max actions per session")
        ("querycost", value<double>(&querycost), "query cost")
        ("invitecost", value<double>(&invitecost),"invite cost")
        ;

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
    {
        cout << desc << "\n";
        return 1;
    }

    if (vm.count("problem") == 0)
    {
        cout << "No problem specified" << endl;
        return 1;
    }

    if (vm.count("test"))
    {
        cout << "Running unit tests" << endl;
        UnitTests();
        return 0;
    }

    SIMULATOR* real = 0;
    SIMULATOR* simulator = 0;

    if (problem == "time")
    {
        Graph *graph = new Graph(graphfile, true, existenceProb, propagationProb);
        graph->makeUndirected();

        //write undirected network in metis format
        char *randStr = new char[10];
        gen_random(randStr,10);
        string undirectFilename(basedir);
        undirectFilename.append(randStr);
        undirectFilename.append(".txt");
        writeAdjacencyList(graph, undirectFilename);

        string command("/home/rcf-40/amulyaya/metis-5.1.0/build/Linux-x86_64/programs/gpmetis ");
        command.append(undirectFilename);
        command.append(" ");
        command.append(NumberToString(numPartitions));
        system(command.c_str());

        ///create a map of partititons
        map<int, map<int, int> > partition;
        for (int i=0;i<numPartitions;i++)
        {
                map<int, int> tep;
                partition.insert(pair<int, map<int, int> >(i, tep));
        }

        string metis_out(undirectFilename);
        metis_out.append(".part.");
        metis_out.append(NumberToString(numPartitions));

        ifstream read;
        read.open(metis_out.c_str());

        if (read.is_open())
        {
                int counter=1;
                int t;
                read>>t;
                while(!read.eof())
                {
                        (partition.find(t)->second).insert(pair<int, int>(counter, 1));
                        counter++;
                        read>>t;
                }
        }

        read.close();
        ////partition constructed


        for (int i=0;i<numPartitions;i++)///create argv[3] partitioned graph files
        {
		string temp(undirectFilename);
                string graph_filename(temp.append("_l_"));
                graph_filename.append(NumberToString(i));
                ofstream file;
                file.open(graph_filename.c_str());

                map<int, int> curr_partition = partition.find(i)->second;

                ifstream reado;
                reado.open(graphfile.c_str());
                if (reado.is_open())
                {
                        int a,b,c,d;
                        reado>>a>>b>>c>>d;
                        while(!reado.eof())
                        {
                                //reado>>a>>b>>c>>d;
                                if (curr_partition.find(a)!=curr_partition.end()&&curr_partition.find(b)!=curr_partition.end())
                                {
                                        file<<a<<" "<<b<<" "<<c<<" "<<d<<"\n";
                                }
                                reado>>a>>b>>c>>d;
                        }
                }

                reado.close();
                reado.clear();
                file.close();
                file.clear();
        }

        ///files created

        ///create actual input files in which node indexes begin from 1
        map<int, map<int, int> > *partitionMap = new map<int, map<int, int> >();
        map<int, map<int, int> >* reversePartitionMap = new map<int, map<int, int> >();

        for (int i=0;i<numPartitions;i++)
        {
		string temp(undirectFilename);
		string graph_filename(temp.append("_l_"));
                graph_filename.append(NumberToString(i));
	
		string temp2(undirectFilename);
        	string out_graph_filename(temp2.append("_part_"));
    		out_graph_filename.append(NumberToString(i));

    		map<int, int> *nodeMap = new map<int, int>();
            map<int, int> *reverseNodeMap = new map<int, int>();
    		int counter=1;
    		ifstream file;
    		file.open(graph_filename.c_str());
    		ofstream out;
    		out.open(out_graph_filename.c_str());
    		int a,b,c,d;
    		while (file>>a>>b>>c>>d)
    		{
        		int write1, write2;
        		if (nodeMap->find(a)==nodeMap->end())
        		{
            			nodeMap->insert(pair<int,int>(a,counter));
                        reverseNodeMap->insert(pair<int, int>(counter, a));
            			write1=counter++;
        		}
        		else
            			write1 = nodeMap->find(a)->second;

        		if (nodeMap->find(b)==nodeMap->end())
        		{
            			nodeMap->insert(pair<int,int>(b,counter));
                        reverseNodeMap->insert(pair<int, int>(counter, b));
				        write2=counter++;
        		}
        		else
            			write2 = nodeMap->find(b)->second;

        		out<<write1<<" "<<write2<<" "<<c<<" "<<d<<"\n";
    		}
    		out.close();
    		file.close();
	
		if (nodeMap->size()<(partition.find(i)->second).size())
		{
			map<int, int> currPartition = (partition.find(i)->second);
			for (map<int, int>::iterator it = currPartition.begin(); it!= currPartition.end(); it++)
			{
				if (nodeMap->find(it->first)==nodeMap->end())
				{
					nodeMap->insert(pair<int, int>(it->first, counter));
					reverseNodeMap->insert(pair<int, int>(counter, it->first));
					counter++;
				}
			}
		}

		if (nodeMap->size()!=(partition.find(i)->second).size())
		{
			cout<<"BRO YOU SILLY\n";
			exit(0);
		}

        	partitionMap->insert(pair<int, map<int, int> >(i, *nodeMap));
            reversePartitionMap->insert(pair<int, map<int, int> >(i, *reverseNodeMap));
        }
        ///actual input files constructed

    //create action map for global graph
    Graph *global_graph = new Graph(graphfile, true, existenceProb, propagationProb);

	vector<Graph *> *graph_list = new vector<Graph *>();

    double *factorProportion = new double[numPartitions];
    int **sizeOfFactorsActionSpace = new int*[numPartitions];
    for (int i=0;i<numPartitions;i++)
        sizeOfFactorsActionSpace[i] = new int[2];

    int* communitySize = new int[numPartitions];

    int NumQueryActions=0;
    int NumInviteActions=0;
	for (int i=0;i<numPartitions;i++)
	{
		string temp(undirectFilename);
		string community_file(temp.append("_part_"));
        	community_file.append(NumberToString(i));
		Graph *p = new Graph(community_file, true, existenceProb, propagationProb);
		graph_list->push_back(p);
	
	factorProportion[i] = (double)((partition.find(i)->second).size()) /(double)( (global_graph->numNodes));
        //factorProportion[i] = (p->numNodes)/(global_graph->numNodes);

        NumQueryActions = 0;
        NumInviteActions = 0;

	
	int sizeofCommunity = (partition.find(i)->second).size();
        for(int j = 0; j <= sizeofCommunity; ++j) // Counting query actions
	{
            NumQueryActions += combination(sizeofCommunity, j);
	}

        for(int j = 0; j <= sizeofCommunity; ++j)    // Counting invite actions
	{
            NumInviteActions += combination(sizeofCommunity, j);
	}
        ///No end session action in partitions
        
        communitySize[i] = sizeofCommunity;
//cout << "Size of community " << i << ": " << sizeofCommunity << endl;
        sizeOfFactorsActionSpace[i][0] = NumQueryActions;
        sizeOfFactorsActionSpace[i][1] = NumInviteActions;
	}


    //set MCTS parameters
    searchParams.numFactors = numPartitions;
    searchParams.sizeofFactorTables = sizeOfFactorsActionSpace;
    searchParams.numNodesinGlobalGraph = global_graph->numNodes;
    searchParams.factorAlphas = factorProportion;
    searchParams.partitionMap = partitionMap;
    searchParams.reversePartitionMap = reversePartitionMap;
    searchParams.MAX_PER_QUERY = MAX_PER_QUERY;
    searchParams.MAX_INVITE = MAX_INVITE;
    searchParams.communitySizes = communitySize;
    searchParams.maxActions = actionsinSession;

    ifstream mnfile;
    mnfile.open(markovnetfile.c_str());

    MARKOV_NET mn;
	 

    if (mnfile.is_open())
    {
        int numNodes, numFactors;	
        mnfile>>numNodes>>numFactors;
        mn.num_vars = numNodes;
        mn.Factors.resize(numFactors);
        for (int i=0;i<numFactors;i++)
        {
            int numTokens;
            mnfile>>numTokens;

            int numVals = 1<<numTokens;
            
            FACTOR* factor = new FACTOR();
            for (int j=0;j<numTokens;j++)
            {
                int factorVar;
                mnfile>>factorVar;
                factor->scope.push_back(factorVar);
            }
            for (int j=0;j<numVals;j++)
            {
                double potentialVal;
                mnfile>>potentialVal;
                factor->potential.push_back(potentialVal);
            }
            mn.Factors[i] = *factor;
        }

    }
    else
    {
      cout<<"File nahi padhi\n";
      assert(0);
    }

    // BELIEF_STATE initBelief;

    // initBelief.presence_belief = &mn;
    // initBelief.invited.clear();
    // initBelief.actions_left = actionsinSession;
    // initBelief.session_index = 1;

    real = new INFLUENCEMAX(global_graph->numNodes, MAX_PER_QUERY, MAX_INVITE, NUM_SESSIONS, actionsinSession, mn, querycost, invitecost, global_graph);
    simulator = new INFLUENCEMAX(global_graph->numNodes, MAX_PER_QUERY, MAX_INVITE, NUM_SESSIONS, actionsinSession, mn, querycost, invitecost, global_graph);

    // (searchParams.initialBelief).Copy(initBelief, *simulator);

    }
    else 
    {
        cout << "Unknown problem" << endl;
        exit(1);
    }


    simulator->SetKnowledge(knowledge);
    EXPERIMENT experiment(*real, *simulator, outputfile, expParams, searchParams);
    experiment.DiscountedReturn();

    delete real;
    delete simulator;
    return 0;
}
