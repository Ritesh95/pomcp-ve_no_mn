#include "mcts.h"
#include "influencemax.h"
#include <math.h>

#include <algorithm>

using namespace std;
using namespace UTILS;

//-----------------------------------------------------------------------------


MCTS::PARAMS::PARAMS()
:   Verbose(0),
    MaxDepth(100),
    NumSimulations(1000),
    NumStartStates(1000),
    UseTransforms(true),
    NumTransforms(0),
    MaxAttempts(0),
    ExpandCount(1),
    ExplorationConstant(1),
    UseRave(false),
    RaveDiscount(1.0),
    RaveConstant(0.01),
    DisableTree(false)
{
}

MCTS::MCTS(const SIMULATOR& simulator, const PARAMS& params)
:   Simulator(simulator),
    Params(params),
    TreeDepth(0)
{
    //VNODE::NumChildren = Simulator.GetNumActions();
    //QNODE::NumChildren = Simulator.GetNumObservations();
    
    VNODE::numFactors = Params.numFactors;
    VNODE::sizeofFactorTables = Params.sizeofFactorTables;

    Root = ExpandNode(Simulator.CreateStartState());

    for (int i = 0; i < Params.NumStartStates; i++)
        Root->BeliefState.AddSample(Simulator.CreateStartState());

    /*cout<<"Printing partitionMap in MCTS\n";
    for (int i=0;i<Params.numFactors; i++)
    {
        map<int, int> currMap = Params.partitionMap->find(i)->second;
	cout<<"Partition "<<i<<"\n";
        for (map<int, int>::iterator it = currMap.begin(); it!= currMap.end();it++)
	{
		cout<<it->first<<" "<<it->second<<"\n";
	}
    }

    cout<<"Printing reversepartitionMap in MCTS\n";
    for (int i=0;i<Params.numFactors; i++)
    {
        map<int, int> currMap = Params.reversePartitionMap->find(i)->second;
        cout<<"Partition "<<i<<"\n";
        for (map<int, int>::iterator it = currMap.begin(); it!= currMap.end();it++)
        {
                cout<<it->first<<" "<<it->second<<"\n";
        }
    }*/

}

MCTS::~MCTS()
{
    VNODE::Free(Root, Simulator);
    VNODE::FreeAll();
}

bool MCTS::Update(boost::multiprecision::cpp_int action, boost::multiprecision::cpp_int observation, double reward)
{
    History.Add(action, observation);
    BELIEF_STATE beliefs;

    // Find matching vnode from the rest of the tree
    VNODE *vnode=0;
    map<pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>, VNODE * >::iterator it = (Root->children).find(pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>(action, observation));

    if (it != (Root->children).end())
       vnode = it->second;
    else
    {
        if (Params.Verbose >= 1)
            cout << "No matching node found" << endl;
    }

    if (vnode)
    {
        if (Params.Verbose >= 1)
            cout << "Matched " << vnode->Beliefs().GetNumSamples() << " states" << endl;
        beliefs.Copy(vnode->Beliefs(), Simulator);
    }

    // If we still have no particles, fail
    if (beliefs.Empty() && (!vnode || vnode->Beliefs().Empty()))
        return false;


    // Find a state to initialise prior (only requires fully observed state)
    const STATE* state = 0;
    state = beliefs.CreateSample(Simulator); 
   
    // Delete old tree and create new root
    VNODE::Free(Root, Simulator);

    VNODE* newRoot = ExpandNode(state);

    (newRoot->BeliefState).Copy(beliefs, Simulator);
    //newRoot->BeliefState = beliefs;

    Root = newRoot;

    return true;
}

boost::multiprecision::cpp_int MCTS::SelectAction()
{
    //if (Params.DisableTree)
    //    RolloutSearch();
    //else
        UCTSearch();
    return GreedyUCB(Root, false);
}


void MCTS::UCTSearch()
{
    ClearStatistics();
    int historyDepth = History.Size();

    for (int n = 0; n < Params.NumSimulations; n++)
    {
//cout << "Simulation " << n << endl;

        STATE* state = Root->Beliefs().CreateSample(Simulator);
        Simulator.Validate(*state);
        Status.Phase = SIMULATOR::STATUS::TREE;
        if (Params.Verbose >= 2)
        {
            cout << "Starting simulation" << endl;
            Simulator.DisplayState(*state, cout);
        }

        TreeDepth = 0;
        PeakTreeDepth = 0;
        double totalReward = SimulateV(*state, Root);
        StatTotalReward.Add(totalReward);
        StatTreeDepth.Add(PeakTreeDepth);

        if (Params.Verbose >= 2)
            cout << "Total reward = " << totalReward << endl;
        //if (Params.Verbose >= 3)
            //DisplayValue(4, cout);

        Simulator.FreeState(state);
        History.Truncate(historyDepth);
    }

    //DisplayStatistics(cout);
}

double MCTS::SimulateV(STATE& state, VNODE* vnode)
{
    boost::multiprecision::cpp_int action = GreedyUCB(vnode, true);

    PeakTreeDepth = TreeDepth;
    if (TreeDepth >= Params.MaxDepth) // search horizon reached
        return 0;

    ///Adding particle to next belief...replace with our code
    if (TreeDepth == 1)
      AddSample(vnode, state);
    
    boost::multiprecision::cpp_int observation;
    double immediateReward, delayedReward = 0;

    bool terminal = Simulator.Step(state, action, observation, immediateReward);
    History.Add(action, observation);

    map<pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>, VNODE * >::iterator it = (vnode->children).find(pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>(action, observation));
    
    VNODE* childVnode=NULL;
    if (!terminal)
    {
        if (it == (vnode->children).end())
        {
            if (vnode->Count >=Params.ExpandCount)
            {
                childVnode = ExpandNode(&state);
                (vnode->children).insert(pair< pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>, VNODE * >(pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>(action, observation), childVnode));
            }
        }
        else
            childVnode = it->second;
    }


    if (!terminal)
    {
        TreeDepth++;
        if (childVnode)
            delayedReward = SimulateV(state, childVnode);
        else
	{
            delayedReward = Rollout(state);
	}
        TreeDepth--;
    }

    double totalReward = immediateReward + Simulator.GetDiscount() * delayedReward;
    
    //chop up the total reward into factors

    //check if it a query action or invite action or end session action
    boost::multiprecision::cpp_int temp1 = 1;

    bool end_sessionbit = (bool)((action & (temp1<<(Params.numNodesinGlobalGraph+1))) !=0);
    bool invite_bit = (action & (temp1<<Params.numNodesinGlobalGraph)) !=0;
    if (end_sessionbit==1)
        vnode->endSessionVal.Add(totalReward);
    else
    {
        int factorAction[Params.numFactors];
        for (int i=0;i<Params.numFactors;i++)
        {
            factorAction[i]=0;
            map<int, int> currPartitionMap = Params.partitionMap->find(i)->second;
        
            for (map<int, int>::iterator partIterator = currPartitionMap.begin(); partIterator!=currPartitionMap.end(); partIterator++)
            {
                bool isNodeSet = (action & (temp1<<(partIterator->first-1))) !=0;
                if (isNodeSet)
                {
                    factorAction[i] = factorAction[i] | (1<<(partIterator->second-1));
                }
            }
        

            if (!invite_bit)//query action
       	    {
                vnode->factorQueryVals[i].at(factorAction[i]).Add(Params.factorAlphas[i] * totalReward);
            }
            else
            {
                vnode->factorInviteVals[i].at(factorAction[i]).Add(Params.factorAlphas[i] * totalReward);
            }
	}
    }

    vnode->Count++;
    
    //QNODE& qnode = vnode->Child(action);
    //double totalReward = SimulateQ(state, qnode, action);
    //vnode->Value.Add(totalReward);
    //AddRave(vnode, totalReward);
    return totalReward;
}


VNODE* MCTS::ExpandNode(const STATE* state)
{
    VNODE* vnode = VNODE::Create(Simulator.Copy(*state));
    //vnode->Value.Set(0, 0);
    Simulator.Prior(state, History, vnode, Status);

    if (Params.Verbose >= 2)
    {
        cout << "Expanding node: ";
        History.Display(cout);
        cout << endl;
    }

    return vnode;
}

void MCTS::AddSample(VNODE* node, const STATE& state)
{
    STATE* sample = Simulator.Copy(state);
    node->Beliefs().AddSample(sample);
    if (Params.Verbose >= 2)
    {
        cout << "Adding sample:" << endl;
        Simulator.DisplayState(*sample, cout);
    }
}

boost::multiprecision::cpp_int MCTS::GreedyUCB(VNODE* vnode, bool ucb) const
{
    const STATE* sample_state = vnode->BeliefState.GetSample(0);
    const INFLUENCEMAX_STATE& sample_imstate = safe_cast<const INFLUENCEMAX_STATE&>(*sample_state);

///*
    if((sample_imstate.actions_left == 0) || (sample_imstate.invited.size() == Params.MAX_INVITE))
    {
        boost::multiprecision::cpp_int action_id = 1;
        action_id = (action_id << (Params.numNodesinGlobalGraph+1));
//cout << "EndSession is the only possible action" << endl;
        return action_id;
    }
//*/

    int N = vnode->Count;
    double logN = log(N + 1);

    // U values (Q + exploration term) of query (sub)actions, invite (sub)actions & endSession
    vector<vector<double> > queryUVals;
    vector<vector<double> > inviteUVals;
    double endSessionUVal;

    for(vector<vector<VALUE<int> > >::iterator it = vnode->factorQueryVals.begin(); it != vnode->factorQueryVals.end(); ++it)
    {
    	vector<double> tempvec;	// vector for u-values of i^th community
    	for(vector<VALUE<int> >::iterator it2 = it->begin(); it2 != it->end(); ++it2)
    	{
    		// it2 has iterator of counts&value of taking j^th sub-action of the i^th community
    		double q = it2->GetValue();	// Q-value of taking it2
    		int n = it2->Count;	// Count of taking it2 

		double u;
		if(ucb)
    			u = q + FastUCB(N, n, logN);
		else
			u = q;

    		tempvec.push_back(u);
    	}
    	queryUVals.push_back(tempvec);
    }

    for(vector<vector<VALUE<int> > >::iterator it = vnode->factorInviteVals.begin(); it != vnode->factorInviteVals.end(); ++it)
    {
    	vector<double> tempvec;
    	for(vector<VALUE<int> >::iterator it2 = it->begin(); it2 != it->end(); ++it2)
    	{
    		double q = it2->GetValue();
    		int n = it2->Count;

    		double u;
		if(ucb)
			u = q + FastUCB(N, n, logN);
		else
			u = q;
    		tempvec.push_back(u);
    	}
    	inviteUVals.push_back(tempvec);
    }

    if(ucb)
    	endSessionUVal = vnode->endSessionVal.GetValue() + FastUCB(N, vnode->endSessionVal.Count, logN);	// U-value of endSession is it's Q-value + exploration term
    else
    	endSessionUVal = vnode->endSessionVal.GetValue();	// U-value of endSession is it's Q-value if ucb is false

    // All U-value tables have been constructed

    int num_factors = queryUVals.size();

    // Finding the best query action
    vector<double> phi_factor;
    unsigned k = Params.MAX_PER_QUERY;
    
    for(int i = 0; i <= k; ++i) // No penalty for 0 to k, and infty beyond that
        phi_factor.push_back(0);

    vector<vector<vector<int> > > query_best_asgn;  // Will contain argmax for the VE, i.e. query_best_asgn[t][i] gives asgn of vars of t^th communties when remaining variables have i 1s
    for(int t = 0; t < num_factors; ++t)
    {
        vector<vector<int> > factor_specific;
        for(int i = 0; i <= k; ++i)
        {
            vector<int> emptyvec;
            factor_specific.push_back(emptyvec);
        }
        query_best_asgn.push_back(factor_specific);
    }

    for(int t = 0; t < num_factors; ++t)    // Eliminate one community factor at a time (by merging it with phi_factor & then marginalizing)
    {
        // Going to eliminate the t^th factor now
        vector<double> new_factor;
        unsigned this_size = Params.communitySizes[t];

        for(int i = 0; i <= k; ++i) // Construct i^th value of new_factor based on max when exactly i vars of remaining are 1
        {
            // Exactly i of the remaining variables are 1
            unsigned quota_left = k - i;    // We can have (k-i) 1s in the current & previous variables

            double best_value = -Infinity;
            vector<int> best_asgn;

            for(int this_ones = min(quota_left, this_size); this_ones >= 0; --this_ones)
            {
                // Find best combination of exactly this_ones 1s in t^th factor
                vector<int> tempvec;
                for(int a = 0; a < this_size; ++a)
                {
                    if(a > this_size-1-this_ones)
                        tempvec.push_back(1);
                    else
                        tempvec.push_back(0);
                }

                bool end = false;
                while(!end) // iterating over all combinations of this_ones 1s
                {
                    vector<int>* newvec = new vector<int>();
                    for(int a = 0; a < this_size; ++a)
                        newvec->push_back(tempvec[a]);

                    // newvec is a combination!
                    unsigned index = 0;
                    for(int a = 0; a < this_size; ++a)  ////// Assuming that 0 variable goes to LSB!!!
                        index += ((newvec->at(a)) << a);
                    
                    double value_of_combination = queryUVals[t][index] + phi_factor[this_ones+i];
                    if(value_of_combination >= best_value)
                    {
                        best_value = value_of_combination;
                        best_asgn = *newvec;
                    }

                    bool nextPresent = false;
                    for(int a = this_size-1; a >= 1; a--)
                    {
                        if((tempvec[a] == 1) && (tempvec[a-1] == 0))
                        {
                            nextPresent = true;
                            tempvec[a] = 0;
                            tempvec[a-1] = 1;
                            int numOnesAfterPoint = 0;
                            for(int j = a+1; j < this_size; ++j)
                            {
                                if(tempvec[j] == 1)
                                    numOnesAfterPoint++;
                            }
                            for(int j = this_size-1; j >= a+1; j--)
                            {
                                if(numOnesAfterPoint)
                                {
                                    tempvec[j] = 1;
                                    numOnesAfterPoint--;
                                }
                                else
                                    tempvec[j] = 0;
                            }
                            break;
                        }
                        else
                            continue;
                    }

                    if(!nextPresent)
                        end = true;
                }
            }

            new_factor.push_back(best_value);
            assert(new_factor[i] == best_value);

            query_best_asgn[t][i] = best_asgn;
        }

        phi_factor = new_factor;
    }


    double max_query_value = phi_factor[0];


    // Finding the best invite action
    phi_factor.clear();
    k = Params.MAX_INVITE - sample_imstate.invited.size();

    for(int i = 0; i <= k; ++i) // No penalty for 0 to k
        phi_factor.push_back(0);

    vector<vector<vector<int> > > invite_best_asgn;
    for(int t = 0; t < num_factors; ++t)
    {
        vector<vector<int> > factor_specific;
        for(int i = 0; i <= k; ++i)
        {
            vector<int> emptyvec;
            factor_specific.push_back(emptyvec);
        }
        invite_best_asgn.push_back(factor_specific);
    }

    for(int t = 0; t < num_factors; ++t)    // Eliminate one community factor at a time
    {
        // Eliminating the t^th factor now
        vector<double> new_factor;
        unsigned this_size = Params.communitySizes[t];

        for(int i = 0; i <= k; ++i) // Populate new_factor
        {
            // Exactly i of the remaining variables are 1
            unsigned quota_left = k - i;

            double best_value = -Infinity;
            vector<int> best_asgn;

            for(int this_ones = min(quota_left, this_size); this_ones >= 0; --this_ones)
            {
                // Find best combination of exactly this_ones 1s in t^th factor
                vector<int> tempvec;
                for(int a = 0; a < this_size; ++a)
                {
                    if(a > this_size-1-this_ones)
                        tempvec.push_back(1);
                    else
                        tempvec.push_back(0);
                }

                bool end = false;
                while(!end) // iterating over all combinations of this_ones 1s
                {
                    vector<int>* newvec = new vector<int>();
                    for(int a = 0; a < this_size; ++a)
                        newvec->push_back(tempvec[a]);
                    
                    // newvec is a combination!
                    unsigned index = 0;
                    for(int a = 0; a < this_size; ++a)  ////// Assuming that 0 variable goes to LSB!!!
                        index += ((newvec->at(a)) << a);

                    double value_of_combination = inviteUVals[t][index] + phi_factor[this_ones+i];
                    if(value_of_combination >= best_value)
                    {
                        best_value = value_of_combination;
                        best_asgn = *newvec;
                    }

                    bool nextPresent = false;
                    for(int a = this_size-1; a >= 1; a--)
                    {
                        if((tempvec[a] == 1) && (tempvec[a-1] == 0))
                        {
                            nextPresent = true;
                            tempvec[a] = 0;
                            tempvec[a-1] = 1;
                            int numOnesAfterPoint = 0;
                            for(int j = a+1; j < this_size; ++j)
                            {
                                if(tempvec[j] == 1)
                                    numOnesAfterPoint++;
                            }
                            for(int j = this_size-1; j >= a+1; j--)
                            {
                                if(numOnesAfterPoint)
                                {
                                    tempvec[j] = 1;
                                    numOnesAfterPoint--;
                                }
                                else
                                    tempvec[j] = 0;
                            }
                            break;
                        }
                        else
                            continue;
                    }

                    if(!nextPresent)
                        end = true;
                }
            }

            new_factor.push_back(best_value);
            assert(new_factor[i] == best_value);

            invite_best_asgn[t][i] = best_asgn;
        }

        phi_factor = new_factor;
    }

    double max_invite_value = phi_factor[0];

if(!ucb)
	cout << "Values: " << endSessionUVal << "(endSession) " << max_query_value << "(query) " << max_invite_value << "(invite)" << endl;

    if(endSessionUVal > max(max_query_value, max_invite_value))    // Take endSession action
    {
        boost::multiprecision::cpp_int action_id = 1;
        action_id = (action_id << (Params.numNodesinGlobalGraph+1));
        return action_id;
    }

    if(max_query_value > max_invite_value)  // Take the best query action
    {
        bool* query_action = new bool[Params.numNodesinGlobalGraph];

        vector<int> temp_asgn = query_best_asgn[num_factors-1][0];  // Assignment for the last community, since remaining are 0

        unsigned this_comm_size = Params.communitySizes[num_factors-1];
        assert(this_comm_size == temp_asgn.size());

        for(unsigned j = 0; j < this_comm_size; ++j)
            query_action[((*(Params.reversePartitionMap))[num_factors-1])[j+1] - 1] = temp_asgn[j]; // partitionMap is 1-indexed everywhere, so +1 for community index, +1 for the index within community & finally -1 to get 0-indexed graph node

        unsigned rem = 0;
        for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)  // Counts 1s in the last community
            rem += *it2;

        for(int t = num_factors - 2; t >= 0; t--)
        {
            temp_asgn = query_best_asgn[t][rem];

            this_comm_size = Params.communitySizes[t];
            assert(this_comm_size == temp_asgn.size());

            for(unsigned j = 0; j < this_comm_size; ++j)
                query_action[((*(Params.reversePartitionMap))[t])[j+1] - 1] = temp_asgn[j];   // partitionMap is 1-indexed everywhere, so +1 for community index, +1 for the index within community & finally -1 to get 0-indexed graph node

            for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)
                rem += *it2;
        }

        // query_action now has the best query assignment!
        unsigned num_nodes = Params.numNodesinGlobalGraph;

        boost::multiprecision::cpp_int temp_cppint = 1;
        boost::multiprecision::cpp_int action_id = 0;
        for(int i = 0; i < num_nodes; ++i)  // Assuming first (zeroth) node goes to LSB
        {
            if(query_action[i] == 1)
                action_id += (temp_cppint << i);
        }

        return action_id;
    }

    else
    {
        bool* invite_action = new bool[Params.numNodesinGlobalGraph];

        vector<int> temp_asgn = invite_best_asgn[num_factors-1][0];

        unsigned this_comm_size = Params.communitySizes[num_factors-1];
        assert(this_comm_size == temp_asgn.size());

        for(unsigned j = 0; j < this_comm_size; ++j)
            invite_action[((*(Params.reversePartitionMap))[num_factors-1])[j+1] - 1] = temp_asgn[j];

        unsigned rem = 0;
        for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)  // Counts 1s in the last community
            rem += *it2;

        for(int t = num_factors - 2; t >= 0; t--)
        {
            temp_asgn = invite_best_asgn[t][rem];

            this_comm_size = Params.communitySizes[t];
            assert(this_comm_size == temp_asgn.size());

            for(unsigned j = 0; j < this_comm_size; ++j)
                invite_action[((*(Params.reversePartitionMap))[t])[j+1] - 1] = temp_asgn[j];

            for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)
                rem += *it2;
        }

        // invite_action now has the best invite assignment!
        unsigned num_nodes = Params.numNodesinGlobalGraph;

        boost::multiprecision::cpp_int temp_cppint = 1;
        boost::multiprecision::cpp_int action_id = 1;
        action_id = (action_id << num_nodes);   // Setting the invite bit
        for(int i = 0; i < num_nodes; ++i)  // Assuming first (zeroth) node goes to LSB
        {
            if(invite_action[i] == 1)
                action_id += (temp_cppint << i);
        }

        return action_id;
    }
}

double MCTS::Rollout(STATE& state)
{
    Status.Phase = SIMULATOR::STATUS::ROLLOUT;
    if (Params.Verbose >= 3)
        cout << "Starting rollout" << endl;

    double totalReward = 0.0;
    double discount = 1.0;
    bool terminal = false;
    int numSteps;
    for (numSteps = 0; numSteps + TreeDepth < Params.MaxDepth && !terminal; ++numSteps)
    {
        boost::multiprecision::cpp_int observation;
        double reward;

        boost::multiprecision::cpp_int action = Simulator.SelectRandom(state, History, Status);
        terminal = Simulator.Step(state, action, observation, reward);
        History.Add(action, observation);

        if (Params.Verbose >= 4)
        {
            Simulator.DisplayAction(action, cout);
            Simulator.DisplayObservation(state, observation, cout);
            Simulator.DisplayReward(reward, cout);
            Simulator.DisplayState(state, cout);
        }

        totalReward += reward * discount;
        discount *= Simulator.GetDiscount();
    }

    StatRolloutDepth.Add(numSteps);
    if (Params.Verbose >= 3)
        cout << "Ending rollout after " << numSteps
            << " steps, with total reward " << totalReward << endl;
    return totalReward;
}


double MCTS::UCB[UCB_N][UCB_n];
bool MCTS::InitialisedFastUCB = true;

void MCTS::InitFastUCB(double exploration)
{
    cout << "Initialising fast UCB table... ";
    for (int N = 0; N < UCB_N; ++N)
        for (int n = 0; n < UCB_n; ++n)
            if (n == 0)
                UCB[N][n] = Infinity;
            else
                UCB[N][n] = exploration * sqrt(log(N + 1) / n);
    cout << "done" << endl;
    InitialisedFastUCB = true;
}

inline double MCTS::FastUCB(int N, int n, double logN) const
{
    if (InitialisedFastUCB && N < UCB_N && n < UCB_n)
        return UCB[N][n];

    if (n == 0)
        return Infinity;
    else
        return Params.ExplorationConstant * sqrt(logN / n);
}

void MCTS::ClearStatistics()
{
    StatTreeDepth.Clear();
    StatRolloutDepth.Clear();
    StatTotalReward.Clear();
}

//-----------------------------------------------------------------------------
