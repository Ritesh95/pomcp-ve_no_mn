#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <time.h>
#include <string>
#include "Graph.h"
#include "markovnet.h"
#include <boost/program_options.hpp>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace boost::program_options;

int main(int argc, char *argv[])
{
    srand(time(NULL));
    ifstream file(argv[1]);
    int NUM_NODES = atoi(argv[2]);
    int K = atoi(argv[3]);
    int flip = atoi(argv[4]);
    int *a;
    int num;
    if (flip==1)
    {
        num=K;
	a=new int[K];
    }
    else if (flip==0)
    {
        num=2*K;
	a=new int[2*K];
    }
    double c;
    int index=0;
    //while(file>>a>>b>>c)
    while(!file.eof())
    {
	index++;
        
        for (int i=0;i<num;i++)
            file>>a[i];
        file>>c;

        stringstream ss;
        ss<<"/home/rcf-proj/mj1/amulyaya/pomcp-ve/src/inputs/pa/"<<NUM_NODES<<"/"<<index<<".txt";
        Graph *global_graph = new Graph(ss.str(), true, 1, 0.6);
        double avgReward=0;
        for (int run=0;run<100;run++)
        {
	     //cout<<"Run "<<run<<"\n";
	     stringstream ss2;
             ss2<<"/home/rcf-proj/mj1/amulyaya/pomcp-ve/src/inputs/pa/"<<NUM_NODES<<"/"<<index<<"_mn.txt";

	        ifstream mnfile;
	    
            mnfile.open(ss2.str().c_str());

            MARKOV_NET mn;



    if (mnfile.is_open())
    {
        int numNodes, numFactors;
        mnfile>>numNodes>>numFactors;
        mn.num_vars = numNodes;
        mn.Factors.resize(numFactors);
        for (int i=0;i<numFactors;i++)
        {
            int numTokens;
            mnfile>>numTokens;

            int numVals = 1<<numTokens;

            FACTOR* factor = new FACTOR();
            for (int j=0;j<numTokens;j++)
            {
                int factorVar;
                mnfile>>factorVar;
                factor->scope.push_back(factorVar);
            }
            for (int j=0;j<numVals;j++)
            {
                double potentialVal;
                mnfile>>potentialVal;
                factor->potential.push_back(potentialVal);
            }
            mn.Factors[i] = *factor;
        }

    }
    else
    {
      cout<<"File nahi padhi\n";
      assert(0);
    }

    bool *presence = new bool[global_graph->numNodes];
    presence = mn.sample_net();

    set<int> invited;
    if (flip==0)
    {
    for (int i=0;i<K;i++)
        if (presence[a[i]]==true)
            invited.insert(a[i]);
    int index_val=K;
    while(invited.size()<K && index_val<2*K)
    {
        if (presence[a[index_val]]==true)
        {
            invited.insert(a[index_val]);
        }
	index_val++;
    }
    }
    else if (flip==1)
    {
        for (int i=0;i<K;i++)
        if (presence[a[i]]==true)
            invited.insert(a[i]);
    }
        


    double MC_Sims = 1000;
    double reward=0;

    double avgInfSpread=0;
    for (double sims=0;sims<MC_Sims;sims++)
    {
                bool *state1 = new bool[NUM_NODES];
                for (int i=0;i<NUM_NODES;i++)
                    state1[i] = 0;
                for(set<int>::iterator it = invited.begin(); it != invited.end(); it++)
                        state1[*it] = 1;

                bool *newState = new bool[NUM_NODES];
                for (int i=0;i<global_graph->T;i++)
                {
                    for (int j=0;j<NUM_NODES;j++)
                        newState[j] = 0;
                    for (int j=0;j<NUM_NODES;j++)
                    {
                        if (state1[j])//node i will spread influence
                        {
                            for (int k=0;k<NUM_NODES;k++)
                            {
                                if (j!=k && global_graph->adjMatrix[j][k]!=0 && !newState[k] && !state1[k])//k is uninfluenced neighbor
                                {
                                    double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                                    if (r < global_graph->existenceProb * global_graph->propagationProb)
                                        newState[k]=1;
                                    else
                                        newState[k]=0;
                                }
                            }
                        }
                    }
                    for (int j=0;j<NUM_NODES;j++)
                        if (newState[j])
                            state1[j]=1;
                }

                double infSpread = 0;
                for (int i=0;i<NUM_NODES;i++)
                    infSpread += state1[i];

                avgInfSpread += infSpread;
     }
		
            reward = avgInfSpread/MC_Sims;
            //cout<<"reward "<<reward<<"\n";
            avgReward+=reward;
        }
        avgReward = avgReward/(double)100;

        for (int i=0;i<K;i++)
            cout<<a[i]<<" ";
        cout<<avgReward<<"\n";
        //cout<<a<<" "<<b<<" "<<avgReward<<"\n";

    }
    file.close();

}
