#ifndef MCTS_H
#define MCTS_H

#include<map>
#include "simulator.h"
#include "node.h"
#include "statistic.h"
#include <boost/multiprecision/cpp_int.hpp>


class MCTS
{
public:

    struct PARAMS
    {
        PARAMS();

        int Verbose;
        int MaxDepth;
        int NumSimulations;
        int NumStartStates;
        bool UseTransforms;
        int NumTransforms;
        int MaxAttempts;
        int ExpandCount;
        double ExplorationConstant;
        bool UseRave;
        double RaveDiscount;
        double RaveConstant;
        bool DisableTree;

        int numFactors;
        int **sizeofFactorTables;
        double *factorAlphas;
        int numNodesinGlobalGraph;
        int *communitySizes;

        int MAX_PER_QUERY;
        int MAX_INVITE;
        int maxActions;

	BELIEF_STATE initialBelief;
        
        map<int, map<int, int> > *partitionMap;
        map<int, map<int, int> > *reversePartitionMap;
    };

    MCTS(const SIMULATOR& simulator, const PARAMS& params);
    ~MCTS();

    boost::multiprecision::cpp_int SelectAction();
    bool Update(boost::multiprecision::cpp_int action, boost::multiprecision::cpp_int observation, double reward);

    void UCTSearch();
    void RolloutSearch();

    double Rollout(STATE& state);

    const BELIEF_STATE& BeliefState() const { return Root->Beliefs(); }
    const HISTORY& GetHistory() const { return History; }
    const SIMULATOR::STATUS& GetStatus() const { return Status; }
    void ClearStatistics();
    //void DisplayStatistics(std::ostream& ostr) const;
    //void DisplayValue(int depth, std::ostream& ostr) const;
    //void DisplayPolicy(int depth, std::ostream& ostr) const;

    static void UnitTest();
    static void InitFastUCB(double exploration);

private:

    const SIMULATOR& Simulator;
    int TreeDepth, PeakTreeDepth;
    PARAMS Params;
    VNODE* Root;
    HISTORY History;
    SIMULATOR::STATUS Status;

    STATISTIC StatTreeDepth;
    STATISTIC StatRolloutDepth;
    STATISTIC StatTotalReward;

    boost::multiprecision::cpp_int GreedyUCB(VNODE* vnode, bool ucb) const;
    double SimulateV(STATE& state, VNODE* vnode);
    void AddRave(VNODE* vnode, double totalReward);
    VNODE* ExpandNode(const STATE* state);
    void AddSample(VNODE* node, const STATE& state);
    void Resample(BELIEF_STATE& beliefs);

    // Fast lookup table for UCB
    static const int UCB_N = 10000, UCB_n = 100;
    static double UCB[UCB_N][UCB_n];
    static bool InitialisedFastUCB;

    double FastUCB(int N, int n, double logN) const;

    static void UnitTestGreedy();
    static void UnitTestUCB();
    static void UnitTestRollout();
    static void UnitTestSearch(int depth);
};

#endif // MCTS_H
