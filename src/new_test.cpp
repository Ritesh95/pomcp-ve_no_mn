#include "Graph.h"
#include "markovnet.h"
#include <boost/program_options.hpp>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace boost::program_options;


int main()
{
	Graph *graph = new Graph(graphfile, true, existenceProb, propagationProb);
    ifstream mnfile;
    mnfile.open(markovnetfile.c_str());

    MARKOV_NET mn;


    if (mnfile.is_open())
    {
        int numNodes, numFactors;
        mnfile>>numNodes>>numFactors;
        mn.num_vars = numNodes;
        mn.Factors.resize(numFactors);
        for (int i=0;i<numFactors;i++)
        {
            int numTokens;
            mnfile>>numTokens;

            int numVals = 1<<numTokens;

            FACTOR* factor = new FACTOR();
            for (int j=0;j<numTokens;j++)
            {
                int factorVar;
                mnfile>>factorVar;
                factor->scope.push_back(factorVar);
            }
            for (int j=0;j<numVals;j++)
            {
                double potentialVal;
                mnfile>>potentialVal;
                factor->potential.push_back(potentialVal);
            }
            mn.Factors[i] = *factor;
        }

    }
    else
    {
      cout<<"File nahi padhi\n";
      assert(0);
    }

	bool *presence = new bool[graph->numNodes];
	presence = MN.sample_net();
	
	
	
	

}
