#ifndef NODE_H
#define NODE_H

#include "beliefstate.h"
#include "utils.h"
#include<utility>
#include <iostream>
#include<bitset>
#include<vector>
#include<map>
#include <boost/multiprecision/cpp_int.hpp>


using namespace std;


class HISTORY;
class SIMULATOR;
//class QNODE;
class VNODE;

//-----------------------------------------------------------------------------
// Efficient computation of value from alpha vectors
// Only used for explicit POMDPs
struct ALPHA
{
    std::vector<double> AlphaSum;
    double MaxValue;
};

//-----------------------------------------------------------------------------

template<class COUNT>
class VALUE
{
public:

    void Set(double count, double value)
    {
        Count = count;
        Total = value * count;
    }

    void Add(double totalReward)
    {
        Count += 1.0;
        Total += totalReward;
    }

    void Add(double totalReward, COUNT weight)
    {
        Count += weight;
        Total += totalReward * weight;
    }

    double GetValue() const
    {
        return Count == 0 ? Total : Total / Count;
    }

    COUNT GetCount() const
    {
        return Count;
    }


    COUNT Count;
    double Total;
};

//-----------------------------------------------------------------------------


class VNODE : public MEMORY_OBJECT
{
public:
    static int numFactors;
    static int **sizeofFactorTables;

    vector<vector<VALUE<int> > >factorQueryVals;
    vector<vector<VALUE<int> > >factorInviteVals;

    VALUE<int> endSessionVal;

    int Count;

    map<pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>, VNODE* > children;
    
    void Initialise();
    static VNODE* Create();
    static VNODE* Create(STATE* state);
    static void Free(VNODE *node, const SIMULATOR& simulator);
    static void FreeAll();

    BELIEF_STATE Beliefs() {return BeliefState;}

    VNODE*& Child(boost::multiprecision::cpp_int act, boost::multiprecision::cpp_int obs) { return children.find(pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>(act, obs))->second; }
    VNODE* Child(boost::multiprecision::cpp_int act, boost::multiprecision::cpp_int obs) const { return children.find(pair<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>(act, obs))->second; }

    void SetChildren(int count, double value);
    void SetEndSessionChildren(int count, double value);

    BELIEF_STATE BeliefState;
    static MEMORY_POOL<VNODE> VNodePool;
};


#endif // NODE_H
